#!/bin/bash
export DOLLAR='$'
envsubst '${VIRTUAL_HOST} ${PUBLIC_DIRECTORY}' < /etc/nginx/conf.d/app.conf.template > /etc/nginx/conf.d/default.conf

if [ -f /etc/nginx/.env ]
then
  export $(cat /etc/nginx/.env | xargs)
fi
echo "$LOCAL $LOCAL_OCTANE";

if [[ "$LOCAL_OCTANE" = true && "$LOCAL" = true ]]; then
    envsubst '${LOCAL_INDEX} ${PUBLIC_DIRECTORY}' < /etc/nginx/stubs/octane.stub > /etc/nginx/conf.d/applocalhost.conf
elif [[  "$LOCAL" = true  ]]; then
    envsubst '${LOCAL_INDEX} ${PUBLIC_DIRECTORY}' < /etc/nginx/stubs/php.stub > /etc/nginx/conf.d/applocalhost.conf
fi

exec "$@"

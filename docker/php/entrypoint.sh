#!/bin/bash
export DOLLAR='$'

if [ -f /usr/local/etc/php/.env ]
then
  export $(cat /usr/local/etc/php/.env | xargs)
fi

if [[ "$LOCAL" = true && "$LOCAL_XDEBUG" = true && -f "/usr/local/etc/php/docker-php-ext-xdebug.ini" ]]; then
    mv /usr/local/etc/php/docker-php-ext-xdebug.ini /usr/local/etc/php/conf.d/docker-php-ext-xdebug.ini

elif [[ "$LOCAL" = true && "$LOCAL_XDEBUG" = false && -f "/usr/local/etc/php/conf.d/docker-php-ext-xdebug.ini" ]]; then
    mv /usr/local/etc/php/conf.d/docker-php-ext-xdebug.ini /usr/local/etc/php/docker-php-ext-xdebug.ini
fi

if [[ "$LOCAL" = true && -f "/usr/local/etc/php/conf.d/docker-php-ext-xdebug.ini" ]]; then
    echo  "xDebug is enabled"
elif [[ "$LOCAL" = true && -f "/usr/local/etc/php/docker-php-ext-xdebug.ini" ]]; then
    echo  "xDebug is disabled"
fi

if [[ "$LOCAL_OCTANE" = true  ]]; then
    exec /usr/bin/supervisord -c /etc/supervisor/conf.d/supervisord.app.conf
else [[  "$LOCAL" = true  ]];
   exec "$@"
fi



#set -e
#if [[ ! -z "$LOCAL_XDEBUG" && "$LOCAL_XDEBUG" = true && -f "/usr/local/etc/php/conf.d/docker-php-ext-xdebug.ini" ]]; then
#mv /usr/local/etc/php/conf.d/docker-php-ext-xdebug.ini /usr/local/etc/php/conf.d/docker-php-ext-xdebug.ini.back
#echo  "xDebug is disabled"
#
#elif [[ -z "$LOCAL_XDEBUG" || "$LOCAL_XDEBUG" = false && -f "/usr/local/etc/php/conf.d/docker-php-ext-xdebug.ini.back" ]]; then
#mv /etc/php/5.6/mods-available/docker-php-ext-xdebug.ini.back /etc/php/5.6/mods-available/docker-php-ext-xdebug.ini
#echo  "xDebug is enabled"
#fi

#exec "$@"




#
#container_mode=${CONTAINER_MODE:-app}
#octane_server=${OCTANE_SERVER:-swoole}
#echo "Container mode: $container_mode"
#
#php() {
#  su www-data -c "php $*"
#}
#
#initialStuff() {
#    php /envelapp/app/octane/artisan optimize:clear; \
#    php /envelapp/app/octane/artisan package:discover --ansi; \
#    php /envelapp/app/octane/artisan event:cache; \
#    php /envelapp/app/octane/artisan config:cache; \
#    php /envelapp/app/octane/artisan route:cache;
#}
#
#if [ "$1" != "" ]; then
#    exec "$@"
#elif [ ${container_mode} = "app" ]; then
#    echo "Octane server: $octane_server"
#    initialStuff
#    exec /usr/bin/supervisord -c /etc/supervisor/conf.d/supervisord.app.conf
#elif [ ${container_mode} = "scheduler" ]; then
#    initialStuff
#    exec supercronic /etc/supercronic/laravel
#else
#    echo "Container mode mismatched."
#    exit 1
#fi
